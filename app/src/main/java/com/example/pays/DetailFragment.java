package com.example.pays;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.pays.data.Country;

import static com.example.pays.data.Country.countries;

public class DetailFragment extends Fragment {

    public static final String TAG = "SecondFragment";
    TextView countryName;
    EditText countryCapitale;
    EditText countryLangue;
    EditText countryMonnaie;
    EditText countryPopulation;
    EditText countrySuperficie;
    ImageView countryDrapeau;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_detail, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        countryDrapeau = view.findViewById(R.id.Drapeau);
        countryName = view.findViewById(R.id.Name);
        countryCapitale = view.findViewById(R.id.Capitale2);
        countryLangue = view.findViewById(R.id.Langues2);
        countryMonnaie = view.findViewById(R.id.Monnaie2);
        countryPopulation = view.findViewById(R.id.Population2);
        countrySuperficie = view.findViewById(R.id.Superficie2);

        DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());

        String uri = countries[args.getCountryId()].getImgUri();
        Context c = countryDrapeau.getContext();
        countryDrapeau.setImageDrawable(c.getResources().getDrawable(c.getResources(). getIdentifier (uri, null , c.getPackageName())));


        countryName.setText(countries[args.getCountryId()].getName());
        countryCapitale.setText(countries[args.getCountryId()].getCapital());
        countryLangue.setText(countries[args.getCountryId()].getLanguage());
        countryMonnaie.setText(countries[args.getCountryId()].getCurrency() + "");
        countryPopulation.setText(countries[args.getCountryId()].getPopulation() + "");
        countrySuperficie.setText(countries[args.getCountryId()].getArea() + "km2");



        view.findViewById(R.id.button_second).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }
}